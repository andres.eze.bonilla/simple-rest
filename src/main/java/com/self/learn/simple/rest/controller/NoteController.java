package com.self.learn.simple.rest.controller;

import com.self.learn.simple.rest.model.Note;
import com.self.learn.simple.rest.service.NoteService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@AllArgsConstructor
@RequestMapping("/api/v1/note")
public class NoteController {

    public final NoteService noteService;

    @PostMapping
    public Note PostNote(@RequestBody Note note){
        return noteService.saveNote(note);
    }
}
