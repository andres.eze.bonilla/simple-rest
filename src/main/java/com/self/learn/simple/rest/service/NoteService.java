package com.self.learn.simple.rest.service;

import com.self.learn.simple.rest.model.Note;
import com.self.learn.simple.rest.repository.NoteRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class NoteService {

    public final NoteRepository noteRepository;

    public Note saveNote(Note note){
        return noteRepository.save(note);
    }

}
