package com.self.learn.simple.rest.repository;

import com.self.learn.simple.rest.model.Note;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface NoteRepository extends MongoRepository<Note, String> {
}
